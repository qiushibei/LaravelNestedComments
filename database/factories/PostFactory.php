<?php

use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'user_id' => array_random(\App\User::pluck('id')->toArray()),
        'title' => $faker->sentence,
        'content' => $faker->paragraph,
    ];
});
