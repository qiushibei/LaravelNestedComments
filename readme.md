

## 基于Laravel的无限嵌套评论用例

无限级别评论回复用例，简单清晰。
参考了LaravelChen的文章[Laravel 5~嵌套评论的实现](https://laravel-china.org/articles/5556/implementation-of-nested-comments-in-laravel5)，文章中有一点小bug，这里已经修复了。

## 安装方法

1. 把项目克隆到本地
2. 在项目文件夹下执行：composer update
3. 创建数据库，配置.env文件中的数据库连接
3. 执行php artisan migrate
4. 执行php artisan db:seed
5. 创建站点
6. 访问 `/posts/1`
7. 没有控制器代码，代码在模型文件和路由文件web.php里

## License

[MIT license](http://opensource.org/licenses/MIT).
