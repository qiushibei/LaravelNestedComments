<div class="reply-form-wrap">
    <a href="javascript:;" class="btn-show-reply">回复</a>
    <div class="form-wrap" style="display: none;">
        <form method="POST" action="{{url('posts/'.$post->id.'/comments')}}" accept-charset="UTF-8">
            {{csrf_field()}}

            @if(isset($parentId))
                <input type="hidden" name="parent_id" value="{{$parentId}}">
            @endif

            <div class="form-group">
                <label for="body" class="control-label">Info:</label>
                <textarea id="body" name="body"  class="form-control" required="required"></textarea>
            </div>
            <button type="submit" class="btn btn-success">回复</button>
        </form>
    </div>
</div>

<script>
    $(function () {
        //显示回复表单
        $('.btn-show-reply').on('click', function (e) {
            $(this).parent().find('.form-wrap').show();
        });

        //提交后隐藏回复表单
        $('form').on('submit', function (e) {
            $(this).parent().hide();
        });
    });
</script>