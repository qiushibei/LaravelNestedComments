<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

\Auth::loginUsingId(1); //用户id为1的登录

Route::get('/', function () {
    return view('welcome');
});

//显示文章和相应的评论
Route::get('/posts/{post}', function (\App\Post $post) {
    $post->load('comments.owner');
    $comments = $post->getComments();
    $comments['root'] = isset($comments['']) ? $comments[''] : [];
    unset($comments['']);
    return view('posts.show', compact('post', 'comments'));
});

//用户进行评论
Route::post('posts/{post}/comments', function (\App\Post $post) {
    $post->comments()->create([
        'body' => request('body'),
        'user_id' => \Auth::id(),
        'parent_id' => request('parent_id', null),
    ]);
    return back();
});
